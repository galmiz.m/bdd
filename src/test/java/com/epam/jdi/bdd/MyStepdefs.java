package com.epam.jdi.bdd;

import com.epam.jdi.light.elements.composite.WebPage;
import com.epam.jdi.light.elements.pageobjects.annotations.FindBy;
import com.epam.jdi.light.elements.pageobjects.annotations.Frame;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import cucumberTests.TestRunner;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;


import static com.epam.jdi.light.elements.init.entities.collection.EntitiesCollection.getPage;

public class MyStepdefs {


    static WebDriver driver;
    @Frame(".truste_popframe")@FindBy(xpath = "//a[@class='call']") public static WebElement popUpAcceptButton;

     //  @Frame(".truste_popframe") public static WebElement frame;
    @FindBy(xpath = "//iframe[@class ='truste_popframe']")
    private WebElement iframe;




    @When("I click myButton")
    public void iClickMyButton() throws InterruptedException {
        Thread.sleep(10000);
        driver.switchTo().defaultContent();
        driver.switchTo().frame(iframe);


        try {
            if (popUpAcceptButton.isDisplayed()) popUpAcceptButton.click();
            driver.switchTo().defaultContent();
        } catch (org.openqa.selenium.NoSuchElementException ignored) {
        }

    }

    @When("^I open page$")
    public void iOpenPage() {
        getPage("Login Page").shouldBeOpened();
    }

//    @Then("^I click myButton(\\d+)$")
//    public void iClickMyButton(int arg0) {
//        popUpAcceptButton.click();
//    }
//
//    @Then("^I again click myButton$")
//    public void iAgainClickMyButton() {
//        popUpAcceptButton.click();
//    }
}
